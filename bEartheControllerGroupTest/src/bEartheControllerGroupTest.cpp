/******************************************************/
//       THIS IS A GENERATED FILE - DO NOT EDIT       //
/******************************************************/

#include "Particle.h"
#line 1 "/Users/sheadawson/Particle/projects/bEarth/bEartheControllerGroupTest/src/bEartheControllerGroupTest.ino"
#include <BLE_Group.h>
void setup();
void loop();
void callbackFunc(const char *event, const char *data);
#line 2 "/Users/sheadawson/Particle/projects/bEarth/bEartheControllerGroupTest/src/bEartheControllerGroupTest.ino"
SerialLogHandler logHandler;
BLE_Group *group;

void setup() {
  group = new BLE_Group_Central(1);
  group->subscribe("test", callbackFunc);
}

void loop() {
}

void callbackFunc(const char *event, const char *data) {
  Log.info("Event: %s, Data: %s", event, data);
}