#include <BLE_Group.h>
SerialLogHandler logHandler;
BLE_Group *group;

void setup() {
  group = new BLE_Group_Central(1);
  group->subscribe("test", callbackFunc);
}

void loop() {
}

void callbackFunc(const char *event, const char *data) {
  Log.info("Event: %s, Data: %s", event, data);
}