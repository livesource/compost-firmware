#include "sensors.h"
#include "config.h"
#include <ArduinoJson.h>
#include <DS18B20.h>
#include "math.h"

Sensors sensors;

Sensors::Sensors() {
  delete (ds);
  ds = new DS18B20(2);

  delete (sht);
  sht = new Adafruit_SHT31();
}

void Sensors::setup() {
  Log.info("Setting up Sensors");

  if(Particle.connected()) {
    Particle.function("publishSensorReading", &Sensors::publishReading, this);
  }

  Log.info("Loading SHT30");
  if (!sht->begin(0x44)) {   // Set to 0x45 for alternate i2c addr
    Log.info("Couldn't find SHT30");
  }

  Log.info("Sensors Loaded");
}

void Sensors::autoPublishReading() {
  if (lastPublish == 0 || millis() - lastPublish > config.readingFrequency) {
    publishReading("");
  }
}

void Sensors::test() {
  float aT = shtTemp();
  float aH = shtHumidity();

  char amsg[64];
  snprintf(amsg, sizeof(amsg), "Atmos Temp: %0.2f | Humidity: %0.2f", aT, aH);
  Log.info(amsg);

  for (int i = 1; i <= 5; i++) {
    double temp = dsTemp(i);
    char msg[64];
    snprintf(msg, sizeof(msg), "Sensor %d: %0.2f", i, temp);
    Log.info(msg);
    //Particle.publish("cc/post/error", msg, PRIVATE);
  }
}

int Sensors::publishReading(String args) {
  bool ready = config.loaded;
  if (!ready) {
    Log.info("Can't publish reading, config not loaded yet");
    return -1;
  }

  Log.info("Publishing Reading...");

  lastPublish = millis();
  StaticJsonDocument<400> doc;
  JsonObject root = doc.to<JsonObject>();
  JsonObject temps = root.createNestedObject("t");
  JsonObject atmos = root.createNestedObject("a");
  char output[400];
  double maxTemp = 0;

  for (int i = 1; i <= 5; i++) {
    double temp = dsTemp(i);

    if (isnan(temp) && config.inputIsActive(i)) {
      char msg[64];
      snprintf(msg, sizeof(msg), "Error reading temp for index %d", i);
      // Particle.publish("cc/post/error", msg, PRIVATE);
      Log.error(msg);
      continue;
    }

    if (temp == 85.00 && config.inputIsActive(i)) {
      char msg[64];
      snprintf(msg, sizeof(msg), "Error reading temp for index %d, got 85.00", i);
      // Particle.publish("cc/post/error", msg, PRIVATE);
      Log.error(msg);
      continue;
    }

    char key[4];
    snprintf(key, sizeof(key), "t%d", i);
    temps[key] = temp;
    Log.info("t%d %0.2f", i, temp);
    if (temp > maxTemp) maxTemp = temp;
  }

  atmos["ah"] = shtHumidity();
  atmos["at"] = shtTemp();

  serializeJson(root, output);

  if (strlen(output) > 0) {
    char event[64];
    snprintf(event, sizeof(event), "cc/post/sensor-reading/%d", config.sensorID);
    Particle.publish(event, output, PRIVATE);
    Log.info("Reading Published");
    // group->publish("max-temp", maxTempMessage.c_str());
    return 1;
  } else {
    Log.info("No output from readings");
  }

  return -1;
}

uint8_t *Sensors::getSensorByIndex(int index) {
  if (index == 1) return sensor1;
  if (index == 2) return sensor2;
  if (index == 3) return sensor3;
  if (index == 4) return sensor4;
  if (index == 5) return sensor5;
  if (index == 6) return sensor6;
  if (index == 7) return sensor7;
  if (index == 8) return sensor8;
  if (index == 9) return sensor9;
  if (index == 10) return sensor10;
  return sensor11;
}

float Sensors::dsTemp(int sensorIndex) {
  double temp;
  uint8_t *addr = getSensorByIndex(sensorIndex);
  int i = 0;

  do {
    temp = ds->getTemperature(addr);
    Log.info("sensor %d", sensorIndex);
    Log.info("%f", temp);
  }
  while (!ds->crcCheck() && maxRetry > i++);

  return (i < maxRetry) ? temp : NAN;
}

float Sensors::shtTemp() {
  float t = sht->readTemperature();
  return !isnan(t) ? t : -85.00;
}

float Sensors::shtHumidity() {
  float h = sht->readHumidity();
  return !isnan(h) ? h : -85.00;
}
