#include "utils.h"

int Utils::parseObjectFailedError(const char * error) {
  char msg[64];
  snprintf(msg, sizeof(msg), "parseObject() failed: %s", error);
  Log.error(msg);
  Particle.publish("cc/post/error", msg, PRIVATE);
  return 0;
}
