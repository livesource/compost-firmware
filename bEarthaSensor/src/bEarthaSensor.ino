using namespace std;

#define ARDUINOJSON_ENABLE_ARDUINO_STRING 1 // required for ArdiunoJson to work with String

SerialLogHandler logHandler;
//SYSTEM_MODE(SEMI_AUTOMATIC);

#include "config.h"
#include "sensors.h"
#include "utils.h"


void setup() {
  Log.info("Setting up application");
  sensors.setup();
  config.setup();
  Particle.publishVitals(60 * 60);
}

void loop() { 
  // sensors.test();
  sensors.autoPublishReading();
}

// #include <BLE_Group.h>
// BLE_Group *group;
// group = new BLE_Group_Peripheral(config_tumblerID);