#include "config.h"
#include "utils.h"
#include <string>
#include <ArduinoJson.h>

Config config;

Config::Config() {}

void Config::setup() {
  Particle.function("setConfig", &Config::setFromJson, this);
  Particle.function("requestFromServer", &Config::requestFromServer, this);
  Particle.subscribe(System.deviceID() + "/hook-response/cc/get/config", &Config::onReceived, this, MY_DEVICES);
  
  if (Particle.connected()) {
    Log.info("Loading Config...");
    Particle.publish("cc/get/config", "", PRIVATE);
  } else {
    delay(6000);
    Log.info("Loading Example Config...");
    setFromJson(exampleJson);
  }

  while (!loaded) delay(500);
  Log.info("Config Loaded");
}

int Config::requestFromServer(String data) {
  Particle.publish("cc/get/config", "", PRIVATE);
  Log.info("Requesting Config from server");
  return 1;
}

void Config::onReceived(const char *event, const char *data) {
  setFromJson(String(data));
}

int Config::setFromJson(String data) {
  Log.info("Setting Config");
  DynamicJsonDocument json(2048);
  DeserializationError error = deserializeJson(json, data.c_str());
  if (error) {
    Utils::parseObjectFailedError(error.c_str());
    return 0;
  }

  readingFrequency = json["c"]["rf"];
  tumblerID = json["c"]["tid"];
  sensorID = json["c"]["sid"];

  Log.info("Config.readingFrequency is: %lu", readingFrequency);
  Log.info("Config.tumblerID is: %d", tumblerID);

  JsonArray activeIn = json["c"]["ait"].as<JsonArray>();
  activeInputs.clear();
  for (JsonVariant val : activeIn) {
    activeInputs.push_back(val.as<int>());
  }

  Log.info("Config.activeInputs are:");
  for (int x : activeInputs) Serial.println(x);

  loaded = true;
  return 1;
}

bool Config::inputIsActive(int input) {
  for (int v : activeInputs) if (v == input) return true;
  return false;
}
