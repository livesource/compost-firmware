#pragma once

#include <ArduinoJson.h>
#include <Particle.h>

class Utils {
 public:
  static int parseObjectFailedError(const char * error);
};
