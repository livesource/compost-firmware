#pragma once
#include <Particle.h>
#include <vector>

class Config {
public:
  Config();
  String exampleJson = String("{\"c\" : {\"rf\" : 1200000, \"tid\" : 1, \"ait\" : [ 1, 3, 4, 5, 7 ]}}");
  unsigned long readingFrequency = 60000 * 20;  // 20 mins default
  int sensorID;
  int tumblerID;
  std::vector<int> activeInputs;
  bool loaded = false;
  void onReceived(const char* event, const char* data);
  int setFromJson(String data);
  int requestFromServer(String data);
  bool inputIsActive(int input);
  void setup();
};

extern Config config;
