#pragma once

#include "Particle.h"

class Pulse {
public:
  Pulse();
  unsigned long interval = 1000 * 55; 
  unsigned long duration = 1000; 
  unsigned long last = 0;
  int relayIndex = 4;
  bool on = false;
  void registerModule();
  void setup();
  void check();
  void send();
  void end();
  int testPulse(String args);
  bool testing = false;
};

extern Pulse pulse;
