#pragma once
#include <Particle.h>

class Action {
public:
  Action();
  // ~Action();
  int id;
  bool autoTrigger;
  int autoPriority;
  int minTemp;
  int maxTemp;
  int turn;
  unsigned long turnTime;
  int fan;
  unsigned long fanTime;
  void trigger();
};



