#pragma once

#include <ArduinoJson.h>
#include "Particle.h"
#include "action.h"

class Config {
public:
  Config();
  const char* testConfig = "{\"c\":{\"tid\":1,\"sdid\":\"e00fce6839748f0a5ce08f64\",\"pat\":5000,\"a\":[{\"id\":4,\"at\":1,\"ap\":4,\"mnt\":70,\"mxt\":0,\"t\":1,\"f\":3,\"ttm\":5000,\"ftm\":10000,\"n\":\"Too Hot Turn\"},{\"id\":3,\"at\":1,\"ap\":3,\"mnt\":60,\"mxt\":69,\"t\":1,\"f\":3,\"ttm\":5000,\"ftm\":10000,\"n\":\"Hot Turn\"},{\"id\":2,\"at\":1,\"ap\":2,\"mnt\":55,\"mxt\":59,\"t\":1,\"f\":2,\"ttm\":5000,\"ftm\":10000,\"n\":\"Perfect Turn\"},{\"id\":1,\"at\":1,\"ap\":1,\"mnt\":0,\"mxt\":54,\"t\":0,\"f\":1,\"ttm\":0,\"ftm\":0,\"n\":\"Warming Up\"}]}}";
  unsigned long pauseAfterTurn = 60000 * 1;  // 1 minutes
  int sensorID;
  int tumblerID;
  int inputPins[8] = {A0, A1, A2, A3, A4, A5, D13, D12};
  int numInputPins = 8;
  int defaultTurn = 0;
  int defaultFan = 3;
  int defaultSafetyIssue = 0;
  int defaultEmergencyStop = 0;
  bool autoPilot = false;
  bool loaded = false;
  int totalInputPins();
  int set(String args);
  int load(const char * data);
  int requestFromServer(String data);
  void onReceived(const char* event, const char* data);
  Action getActionByID(int id);
  void registerModule();
  void setup();
};

extern Config config;
