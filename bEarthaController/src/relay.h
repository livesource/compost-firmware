#pragma once

#include "Particle.h"

class Relay {
public:
  Relay();
  int latchMs = 15;
  int onPins[5] = {D0,D2,D4,D6,D8};
  int offPins[5] = {D1,D3,D5,D7,D9};
  int totalPins();
  void setPinModes();
  void reset();
  void switchTurn(int val);
  void switchFan(int val);
  void switchEmergencyStop(int val);
  int switchTurnPF(String args);
  int switchFanPF(String args);
  int switchEmergencyStopPF(String args);
  void toggle(int index, int val);
  void registerModule();  
  void setup();
  void cycleTest();
  void test();
};

extern Relay relay;