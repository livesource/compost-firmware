#include "relay.h"
#include "config.h"

Relay relay;

Relay::Relay() {}

void Relay::registerModule() {
  Log.info("Registering Relays...");
  Particle.function("switchTurn", &Relay::switchTurnPF, this);
  Particle.function("switchFan", &Relay::switchFanPF, this);
  Particle.function("switchEmergencyStop", &Relay::switchEmergencyStopPF, this);
  Log.info("Relays Registration complete");
}

void Relay::setup() {
  Log.info("Setting up Relays...");
  setPinModes();
  reset();
  switchFan(config.defaultFan);
  switchTurn(config.defaultTurn);
  Log.info("Relays setup complete");
}

int Relay::totalPins() {
  return arraySize(onPins);
}

void Relay::setPinModes() {
  for (int i = 0; i < totalPins(); i++) {
    pinMode(offPins[i], OUTPUT);
    pinMode(onPins[i], OUTPUT);
  }
}

// turn all pins off
void Relay::reset() {
  for (int i = 0; i < totalPins(); i++) {
    digitalWrite(offPins[i], HIGH);
    delay(latchMs);
    digitalWrite(offPins[i], LOW);
  }
}

void Relay::switchTurn(int val) {
  // Log.info("Relay::switchTurn (%d)", val);
  if (val == 0 || val == 4) {
    toggle(0, LOW);
    toggle(1, LOW);
  } else if (val == 1) {
    toggle(0, HIGH);
    toggle(1, LOW);
  } else if (val == 2) {
    toggle(0, LOW);
    toggle(1, HIGH);
  }
}

int Relay::switchTurnPF(String args) {
  switchTurn(args.toInt());
  return 1;
}

void Relay::switchFan(int val) {
  // Log.info("Relay::switchFan (%d)", val);
  if (val == 0) {
    toggle(2, LOW);
    toggle(3, LOW);
  } else if (val == 1) {
    toggle(2, HIGH);
    toggle(3, LOW);
  } else if (val == 2) {
    toggle(2, LOW);
    toggle(3, HIGH);
  } else if (val == 3) {
    toggle(2, HIGH);
    toggle(3, HIGH);
  }
}

int Relay::switchFanPF(String args) {
  switchFan(args.toInt());
  return 1;
}

void Relay::switchEmergencyStop(int val) {
  // Log.info("Relay::switchEmergencyStop (%i)", val);

  if (val == 1) {
    toggle(0, 1); // 1
    toggle(1, 0); // 2
    toggle(2, 1); // 3
    toggle(3, 1); // 4
    toggle(4, 1); // 5
  } else {
    switchTurn(config.defaultTurn);
    switchFan(config.defaultFan);
    toggle(4, LOW);
  }
}

int Relay::switchEmergencyStopPF(String args) {
  switchEmergencyStop(args.toInt());
  return 1;
}

void Relay::toggle(int index, int val) {
  if (val == HIGH) {
    // Log.info("relay %d on", index);
    digitalWrite(onPins[index], HIGH);
    delay(latchMs);
    digitalWrite(onPins[index], LOW);
  } else {
    // Log.info("relay %d off", index);
    digitalWrite(offPins[index], HIGH);
    delay(latchMs);
    digitalWrite(offPins[index], LOW);
  }
}

void Relay::cycleTest() {
  int numTest = 4;
  for (int i = 0; i < numTest + 1; i++) {
    if (i == 0) {
      toggle(numTest, LOW);
    } else {
      toggle(i - 1, LOW);
    }

    toggle(i, HIGH);
    delay(7000);
  }
}

void Relay::test() {
  toggle(0, HIGH);
  delay(10000);
  toggle(0, LOW);
  delay(10000);
}
