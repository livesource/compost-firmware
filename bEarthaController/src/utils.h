#pragma once

#include <ArduinoJson.h>
#include "Particle.h"

class Utils {
public:
  static int parseObjectFailedError(DeserializationError& error, const char * data);
};
