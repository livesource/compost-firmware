#pragma once

#include "Particle.h"
#include <ArduinoJson.h>
#include "PLCState.h"

class State {
public:
  State();
  PLCState inputs;
  unsigned long turnStopTime = 0;
  unsigned long fanStopTime = 0;
  unsigned long checkInputsInterval = 2500;  // 2.5 secs
  unsigned long checkTimersInterval = 2500;  // 2.5 secs
  unsigned long lastCheckInputsTime = 0;
  unsigned long lastInputsChangeTime = 0;
  unsigned long debounceInputsTime = 7000; // 7 secs
  unsigned long lastCheckTimersTime = 0;
  unsigned long lastTurnEndTime = 0;
  int maintenance;
  bool sendingPulse;
  bool isAutoEnabled();
  void logLastTurnEndTime();
  void checkTimers();
  void checkInputs(bool forcePublish = false);
  void publish(PLCState PLCState);
  int switchMaintenance(String args);
  PLCState getCurrentInputsState();
  void reset();
  void registerModule();
  void setup();
};

extern State state;
