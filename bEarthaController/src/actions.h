#pragma once

#include <ArduinoJson.h>
#include "Particle.h"
#include "action.h"
#include <vector> 

class Actions {
public:
  Actions();
  void registerModule();
  void setup();
  std::vector<Action> collection;
  Action getActionForMaxTemp(float highestTemp);
  int handleMaxTemp(float highestTemp);
  int handleMaxTempString(String args);
  int triggerFromAPI(String args);
  int triggerByID(int id);
  void loadCollection(JsonArray collection_);
  Action getByID(int id);
  void test();
  void testMaxTemp();
};

extern Actions actions;