#include "state.h"
#include "config.h"
#include "relay.h"

State state;

State::State() {}

void State::registerModule() {
  Log.info("Registering State...");
  Particle.function("switchMaintenance", &State::switchMaintenance, this);
  Log.info("State Registration complete");
}

void State::setup() {
  Log.info("Setting up State...");

  for (int i = 0; i < config.numInputPins; i++) {
    pinMode(config.inputPins[i], INPUT_PULLDOWN);
    pinMode(config.inputPins[i], INPUT_PULLDOWN);
  }

  Log.info("State setup complete");
}

bool State::isAutoEnabled() {
  // autopilot is disabled when the tumbler is turning
  if (!config.autoPilot) {
    Log.info("Autopilot is not enabled in app settings");
    return false;
  }

  // autopilot is disabled when the tumbler is turning
  if (inputs.turn != 0) {
    Log.info("Autopilot disabled. Reason: Turning");
    return false;
  }

  // autopilot is disabled when maintenance mode is on
  if (maintenance != 0) {
    Log.info("Autopilot disabled. Reason: Maintenance");
    return false;
  }

  // autopilot is disabled for the configured period after autopilot turning has finished
  unsigned long pausedUntil = lastTurnEndTime + config.pauseAfterTurn;
  unsigned long ms = millis();
  if (lastTurnEndTime && (pausedUntil > ms)) {
    float pauseMins = config.pauseAfterTurn / 1000 / 60;
    float pauseMinsRemaining = (pausedUntil - ms) / 1000 / 60;
    Log.info(
      "Autopilot disabled. Reason: Settling for %f mins, %f mins remaining",
      pauseMins,
      pauseMinsRemaining
    );
    return false;
  } else {
    Log.info(
      "Autopilot enabled. millis = %u, lastTurnEndTime = %u, pauseAfterTurn = %u", 
      millis(), lastTurnEndTime, config.pauseAfterTurn
    );
  }

  return true;
}

void State::logLastTurnEndTime() {
  lastTurnEndTime = millis();
  Log.info("Logged lastTurnEndTime %u", lastTurnEndTime);
}

void State::checkTimers() {
  if ((config.loaded && lastCheckTimersTime == 0) || (millis() - lastCheckTimersTime >= checkTimersInterval)) {
    //Log.info("Checking Timers - turnStopTime: %d | fanStopTime %d | millis: %d", turnStopTime, fanStopTime, millis());

    if (turnStopTime > 0 && millis() >= turnStopTime) {
      Log.info("Turn timer up (%lu >= %lu), stopping turning", millis(), turnStopTime);
      turnStopTime = 0;
      if (inputs.turn != config.defaultTurn) {
        // stop turning
        relay.switchTurn(config.defaultTurn);
        // additionally we have changed from action initiated turning to not turning
        // so, we want to pause automatic turning based on sensor readings
        logLastTurnEndTime();
      }
    }

    if (fanStopTime > 0 && millis() >= fanStopTime) {
      Log.info("Fan timer up (%lu >= %lu), stopping fanning", millis(), fanStopTime);
      fanStopTime = 0;
      
      if (inputs.fan != config.defaultFan) {
        relay.switchFan(config.defaultFan);
      }
    }
    lastCheckTimersTime = millis();
  }
}

PLCState State::getCurrentInputsState() {
  PLCState newPLCState;
    //Log.info("Checking Inputs");

    int ipTurnIn = digitalRead(A0);
    int ipTurnOut = digitalRead(A1);
    int ipFan1 = digitalRead(A2);
    int ipFan2 = digitalRead(A3);
    int ipFan3 = digitalRead(A4);
    int ipParked = digitalRead(A5);
    int ipMaintenance = digitalRead(D13);
    int ipEmergency = digitalRead(D12);
    Log.info(
      "ipTurnIn: %d | ipTurnOut: %d | ipFan1: %d | ipFan2: %d | ipFan3: %d | ipParked: %d | ipMaintenance: %d | ipEmergency : %d",
      ipTurnIn, ipTurnOut, ipFan1, ipFan2, ipFan3, ipParked, ipMaintenance, ipEmergency
    );

    // turning
    if (ipTurnIn == HIGH) newPLCState.turn = 1;  // turn in
    else if (ipTurnOut == HIGH) newPLCState.turn = 2; // turn out
    else if (ipParked == HIGH) newPLCState.turn = 3; // parked
    else newPLCState.turn = 0; // stationary, not parked

    // fan speed
    if (ipFan3 == HIGH) {
      newPLCState.fan = 3;  // fast
    } else if (ipFan2 == HIGH) {
      newPLCState.fan = 2;  // medium
    } else if (ipFan1 == HIGH) 
      newPLCState.fan = 1; // slow
    else {
      newPLCState.fan = 0; // off
    }

    // maintenance
    newPLCState.maintenance = (ipMaintenance == HIGH);

    // emergency stop
    newPLCState.emergencyStop = (ipEmergency == HIGH);

    return newPLCState;
}

void State::checkInputs(bool forcePublish) {
  if ((config.loaded && lastCheckInputsTime == 0) || (millis() - lastCheckInputsTime >= checkInputsInterval)) {
    // if the last time the input changed was less than 5 seconds ago, skip and let the debounce check
    Log.info("checkInputs0");
    if (lastInputsChangeTime > 0 && (lastInputsChangeTime + debounceInputsTime) > millis()) return;
    Log.info("checkInputs1");
    PLCState newPLCState = getCurrentInputsState();

    if (forcePublish || ! (inputs == newPLCState) ) {
      Log.info("checkInputs2");
      // ok inputs have changed
      // wait 5 seconds and check again. Is it still changed?
      // This check makes sure we don't log any short/insignificant signals 
      // that can result from electrical interferrence etc
      //Particle.publish("cc/log", "Input Change", PRIVATE);
      lastInputsChangeTime = millis();
      bool debounced = false;
      while(!debounced) {
        debounced = lastInputsChangeTime + debounceInputsTime < millis();
      }
      // after 5 sec delay, are the inputs still reading changed? If so, publish
      PLCState debouncePLCState = getCurrentInputsState();
      if (!(inputs == debouncePLCState)) {
        publish(newPLCState);
      } else {
        Log.info("Debounced Input Change");
        Particle.publish("cc/log", "Debounced Input Change", PRIVATE);
      }
    } else {
      // Log.info("Inputs not changed - Turn: %d | Fan: %d", inputs.turn, inputs.fan);
    }

    lastCheckInputsTime = millis();
  }
}

void State::publish(PLCState newPLCState) {
  inputs = newPLCState;

  StaticJsonDocument <200> doc;
  char output[200];
  doc["turn"] = inputs.turn;
  doc["fan"] = inputs.fan;
  doc["emergencyStop"] = inputs.emergencyStop;
  doc["maintenance"] = inputs.maintenance;

  serializeJson(doc, output);

  Particle.publish("cc/post/tumbler-state", output, PRIVATE);
  Log.info("Published inputs state change: %s", output);
}

int State::switchMaintenance(String args) {
  int val = args.toInt();
  if (val == 0 || val == 1) {
    maintenance = val;
    publish(inputs);
    return 1;
  } else {
    return 0;
  }
}

void State::reset() {
  // 
}
