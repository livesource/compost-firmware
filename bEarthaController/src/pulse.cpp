#include "pulse.h"
#include "relay.h"

Pulse pulse;

Pulse::Pulse() {}

void Pulse::registerModule() {
  Log.info("Registering Pulse...");
  Particle.function("testPulse", &Pulse::testPulse, this);
  Log.info("Pulse Registration complete");
}

void Pulse::setup() {}

int Pulse::testPulse(String args) {
  Log.info(args);
  if (args.toInt() == 1) {
    testing = true;
    send();
  } else {
    testing = false;
    end();
  }
  
  return 1;
} 

void Pulse::send() {
  // Log.info("Pulse Start...");
  relay.toggle(relayIndex, HIGH);
  on = true;
}

void Pulse::end() {
  // Log.info("Pulse End...");
  relay.toggle(relayIndex, LOW);
  on = false;
}

void Pulse::check() {
  //return; // disable to see if it is breaking things
  if (testing) return;
  if (last == 0 || (!on && millis() - last >= interval)) {
    send();
    last = millis();
  }

  if (on && millis() - last >= duration) {
    end();
    last = millis();
  }
}