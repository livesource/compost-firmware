#include "sensor.h"
#include "state.h"
#include "actions.h"
#include "utils.h"
#include "config.h"
#include <math.h>

Sensor sensor;

Sensor::Sensor() {}

void Sensor::registerModule() {
  char event[64];
  snprintf(event, sizeof(event), "cc/post/sensor-reading/%d", config.sensorID);
  Particle.subscribe(event, &Sensor::onReadingReceived, this, MY_DEVICES);
  Log.info("Subscribed to %s", event);
}

void Sensor::setup() {}

void Sensor::onReadingReceived(const char * event, const char * data) {
  Log.info("onReadingReceived: %s", data);
  StaticJsonDocument <400> doc;
  DeserializationError error = deserializeJson(doc, data);

  if (error) {
    Utils::parseObjectFailedError(error, data);
    return;
  }

  JsonObject tmps = doc["t"];
  float highestTemp = 0;
  char key[4];
  for (int i = 1; i < 9; i++) {
    snprintf(key, sizeof(key), "t%d", i);
    highestTemp = fmax(tmps[key], highestTemp);
  }
	actions.handleMaxTemp(highestTemp);
}
