#include "action.h"
#include "relay.h"
#include "state.h"

Action::Action () { }

void Action::trigger() {
  Log("Triggering this Action #%d", id);
  // send action to PLC
  relay.switchTurn(turn);
  relay.switchFan(fan);

  // set timers
  state.turnStopTime = (turnTime > 0) ? millis() + turnTime : 0;
  state.fanStopTime = (fanTime > 0) ? millis() + fanTime : 0;

  // notify
  StaticJsonDocument<200> doc;
  char data[100];
  doc["actionID"] = id;
  serializeJson(doc, data);
  Particle.publish("/cc/post/action-triggered", data, PRIVATE);
  Log.info("Triggered Action #%d. (turn: %d | fan: %d | turnTime: %lu so turnStopTime: %lu | fanTime: %lu so fanStopTime %lu", 
  id, turn, fan, turnTime, state.turnStopTime, fanTime, state.fanStopTime);
}