#define ARDUINOJSON_ENABLE_ARDUINO_STRING 1  // required for ArdiunoJson to work with String

// #include <BLE_Group.h>
#include "config.h"
#include "relay.h"
#include "state.h"
#include "sensor.h"
#include "actions.h"
#include "pulse.h"

SerialLogHandler logHandler;

// SYSTEM_MODE(SEMI_AUTOMATIC);

void setup() {
  registerModules();
  setupModules();
  actions.setup();
  config.setup();
  relay.setup();
  sensor.setup();
  pulse.setup();
  state.setup();
  delay(2000); // not sure if this is needed...
  state.checkInputs();

  if (Particle.connected()) {
    Particle.publishVitals(60 * 60);
  }
}

void loop() {
  state.checkInputs();
  state.checkTimers();
  pulse.check();
  // actions.testMaxTemp();
  //relay.cycleTest();
}

void registerModules() {
  actions.registerModule();
  relay.registerModule();
  pulse.registerModule();
  state.registerModule();
  config.registerModule();
  while (!config.loaded) delay(10);
  sensor.registerModule();
}

void setupModules() {
  actions.setup();
  config.setup();
  relay.setup();
  sensor.setup();
  pulse.setup();
  state.setup();
}

//BLE_Group * group;
// group = new BLE_Group_Central(config.tumblerID); // The parameter is the groupID
// //group->subscribe("max-temp", onMaxTempReceived);
