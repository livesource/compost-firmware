#include "utils.h"

int Utils::parseObjectFailedError(DeserializationError& error, const char * data) {
  char msg[64];
  snprintf(msg, sizeof(msg), "parseObject() failed: %s", error.c_str());
  Log.info(msg);
  Log.info(data);
  Particle.publish("cc/post/error", msg, PRIVATE);
  return 0;
}
