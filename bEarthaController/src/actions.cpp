#include <Particle.h>
#include <ArduinoJson.h>
#include "action.h"
#include "actions.h"
#include "config.h"
#include "state.h"

Actions actions;

Actions::Actions() {}

void Actions::registerModule() {
  Log.info("Registering Actions...");
  Particle.function("triggerAction", &Actions::triggerFromAPI, this);
  Particle.function("handleMaxTemp", &Actions::handleMaxTempString, this);
  Log.info("Actions Registration complete");
}

void Actions::setup() {}

Action Actions::getByID(int id) {
  unsigned int i;
  for (i = 0; i < collection.size(); i++) {
    if (id == collection[i].id) return collection[i];
  }
  Action empty;
  return empty;
}

Action Actions::getActionForMaxTemp(float highestTemp) {
  float max, min;
  bool actionMatch = false;
  Action action;

  for (unsigned int i = 0; i < collection.size(); i++) {
    action = collection[i];
    if (action.id == 0) continue;

    max = action.maxTemp;
    min = action.minTemp;
    if ((max == 0 && highestTemp >= min) || (min == 0 && highestTemp <= max) || (highestTemp >= min && highestTemp <= max)) {
      actionMatch = true;
      break;
    }
  }

  if (actionMatch) {
    Log.info("Action matched (#%d) for temp received %f", action.id, highestTemp);
    return action;
  } else {
    Log.info("No action matched for temp received %f", highestTemp);
    Action dummy;
    return dummy;
  }
}

int Actions::handleMaxTemp(float highestTemp) {
  Log.info("Actions::handleMaxTemp(%f)", highestTemp);
  if (!state.isAutoEnabled()) {
    Log.info("Auto not enabled, can\'t handleMaxTemp");
    return 0;
  }

  Action action = getActionForMaxTemp(highestTemp);
  if (action.id) {
    action.trigger();
    return 1;
  } else {
    Log.info("Action for temp %f not found", highestTemp);
    return 0;
  }
}

int Actions::handleMaxTempString(String args) {
  return handleMaxTemp(args.toFloat());
}

int Actions::triggerFromAPI(String args) {
  int id = args.toInt();
  return triggerByID(id);
}

int Actions::triggerByID(int id) {
  Action action = getByID(id);
  if (action.id != 0) {
    action.trigger();
    return 1;
  } 
  return 0;
}

void Actions::test() {
  Log.info("Starting Actions.test()");
  for (unsigned int i = 0; i < collection.size(); i++) {
    collection[i].trigger();
    delay(5000);
  }
}

void Actions::testMaxTemp() {
  delay(5000);
  Log.info("Starting Actions.testMaxTemp()");

  state.lastTurnEndTime = 0;
  config.pauseAfterTurn = 10000;
  Action warmingUpAction = getByID(1);
  Action perfectTurnAction = getByID(2);
  Action hotTurnAction = getByID(3);

  // reset to Warming Up Action
  Log.info("Triggering Warming Up Action");
  warmingUpAction.trigger();
  delay(3000);
  state.checkInputs();
  while (state.inputs.fan != 1 && state.inputs.turn != 0) {
    state.checkInputs();
    delay(3000);
  }

  // // trigger perfect turn
  Log.info("Triggering Perfect Turn Action");
  handleMaxTemp(perfectTurnAction.minTemp + 1);
  while (state.inputs.turn != 1) {
    delay(1000);
    state.checkInputs();
  }

  // // trigger perfect turn while already turning
  Log.info("Triggering handleMaxTemp while turning, should not trigger while turning");
  handleMaxTemp(perfectTurnAction.minTemp + 1);
  delay(2000);

  Log.info("Waiting For Perfect Turn Action to stop Turning (%lu secs)...", perfectTurnAction.turnTime / 1000);
  while (state.turnStopTime != 0) {
    delay(1000);
    state.checkTimers();
  }
  Log.info("Waiting for inputs.turn to turn off...");
  while (state.inputs.turn != config.defaultTurn) {
    delay(2000);
    state.checkInputs();
    Log.info("state.turn is %d | state.fan is %d", state.inputs.turn, state.inputs.fan);
  }

  Log.info("Waiting For Perfect Turn Action to stop Fanning... (%lu seconds)", perfectTurnAction.fanTime / 1000);
  while (state.fanStopTime != 0) {
    delay(1000);
    state.checkTimers();
  }
  Log.info("Waiting for inputs.fan to turn back to 1...");
  while (state.inputs.fan != config.defaultFan) {
    delay(2000);
    state.checkInputs();
    Log.info("state.turn is %d | state.fan is %d", state.inputs.turn, state.inputs.fan);
  }

  if (state.inputs.fan == config.defaultFan){
    Log.info("Perfect Turn Action complete");
  } else {
    Log.info("BAD: Perfect Turn Action should be complete, but fan has not returned to default");
  }

  delay(1000);

  Log.info("Triggering handleMaxTemp while settling, should fail because settling...");
  handleMaxTemp(hotTurnAction.minTemp + 1);
  delay(7000); state.checkInputs(); delay(1000); state.checkTimers(); 

  Log.info("Triggering Hot Turn Action, should trigger because settling complete...");
  handleMaxTemp(hotTurnAction.minTemp + 1);
  state.checkInputs();
  while (state.inputs.fan != hotTurnAction.fan) {
    delay(2000);
    state.checkInputs();
  }

  Log.info("End.");
  delay(1000 * 120);
}