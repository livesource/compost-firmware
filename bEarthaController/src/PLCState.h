#pragma once

#include "config.h"

class PLCState {
public:
  PLCState() {}
  ~PLCState() {}
  int turn = config.defaultTurn;
  int fan = config.defaultFan;
  int maintenance = config.defaultSafetyIssue;
  int emergencyStop = config.defaultEmergencyStop;

  friend bool operator==(const PLCState& a, const PLCState& b) {
    return (
      a.turn == b.turn && 
      a.fan == b.fan && 
      a.emergencyStop == b.emergencyStop && 
      a.maintenance == b.maintenance 
    );
  }
};