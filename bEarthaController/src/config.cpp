#include <ArduinoJson.h>
#include <Particle.h>
#include "action.h"
#include "actions.h"
#include "config.h"
#include "utils.h"
#include <vector>

Config config;

Config::Config() {}

void Config::registerModule() {
  Log.info("Setting up Config...");
  
  if (Particle.connected()) {
    Particle.function("setConfig", &Config::set, this);
    Particle.function("requestFromServer", &Config::requestFromServer, this);
    Particle.subscribe(System.deviceID() + "/hook-response/cc/get/config", &Config::onReceived, this, MY_DEVICES);
    if (!loaded) Particle.publish("cc/get/config", "", PRIVATE);
  } else {
    load(testConfig);
  }

  Log.info("Config Setup complete");
}

void Config::setup() {

}

// Particle.subscribe handler for /hook-response/cc/get/config
void Config::onReceived(const char* event, const char* data) {
  load(data);
}

int Config::requestFromServer(String data) {
  Particle.publish("cc/get/config", "", PRIVATE);
  Log.info("Requesting Config from server");
  return 1;
}

// Particle.function handler for setConfig
int Config::set(String args) {
  return load(args.c_str());
}

// Load config from given json data
int Config::load(const char * data) {
  Log.info("Config.setFromJson...");
  Serial.println(data);

  DynamicJsonDocument json(2048);
  DeserializationError error = deserializeJson(json, data);

  if (error) {
    return Utils::parseObjectFailedError(error, data);
  }

  JsonArray actionsCollection = json["c"]["a"];
  actions.collection.clear();

  for (unsigned int i = 0; i < actionsCollection.size(); i++) {
    JsonObject aData = actionsCollection[i];
    Action item;
    item.id = aData["id"];
    Log.info("Loading action #%d", item.id);
    item.autoTrigger = aData["at"];
    item.autoPriority = aData["ap"];
    item.minTemp = aData["mnt"];
    item.maxTemp = aData["mxt"];
    item.turn = aData["t"];
    item.turnTime = aData["ttm"]; 
    item.fan = aData["f"];
    item.fanTime = aData["ftm"]; 
    
    if (Particle.connected()) {
      item.turnTime = item.turnTime * 60000;
      item.fanTime = item.fanTime * 60000;
    }

    actions.collection.push_back(item);
  }

  sensorID = 1;//json["c"]["sid"];
  tumblerID = json["c"]["tid"];
  pauseAfterTurn = json["c"]["pat"];
  autoPilot = json["c"]["auto"];
  loaded = true;

  json.clear();
  Log.info("Config.setFromJson complete");

  Particle.publish("cc/log", "Config loaded", PRIVATE);

  return 1;
}

int Config::totalInputPins() {
  return arraySize(inputPins);
}
