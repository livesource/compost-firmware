#pragma once

#include <ArduinoJson.h>
#include "Particle.h"

class Sensor {
public:
  Sensor();
  void registerModule();
  void setup();
  void registerCloudFunctions();
  void onReadingReceived(const char * event, const char * data);
};

extern Sensor sensor;
