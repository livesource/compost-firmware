#include <vector>

SYSTEM_MODE(SEMI_AUTOMATIC);
SerialLogHandler logHandler;

unsigned long lastInputCheck = 0;
unsigned long inputCheckInterval = 50;  // 1 secs

const int numIn = 5;
const int numOut = 8;
int in[numIn] = {A1, A2, A3, A4, A5};
int out[numOut] = {D0, D1, D2, D3, D4, D5, D6, D7};
std::array<int, numIn> prevState{LOW, LOW, LOW, LOW, LOW};

void setup() {
  for (size_t i = 0; i < numIn; i++) pinMode(in[i], INPUT_PULLDOWN);
  for (size_t i = 0; i < numOut; i++) pinMode(out[i], OUTPUT);
}

void loop() {  
  // cycleTest();
  if (lastInputCheck == 0 || millis() - lastInputCheck >= inputCheckInterval) {
    checkInput();
  }
}

int digitalReadWithDelay(int pin) {
  delay(100);
  return digitalRead(pin);
}

void checkInput() {
  std::array<int, numIn> curState{
      digitalReadWithDelay(in[0]),
      digitalReadWithDelay(in[1]),
      digitalReadWithDelay(in[2]),
      digitalReadWithDelay(in[3]),
      digitalReadWithDelay(in[4])};

  Log.info("1:%d,2:%d,3:%d,4:%d,5:%d", curState[0], curState[1], curState[2], curState[3], curState[4]);
  if (curState == prevState) return;

  prevState[0] = (curState[0] == LOW) ? LOW : HIGH;
  prevState[1] = (curState[1] == LOW) ? LOW : HIGH;
  prevState[2] = (curState[2] == LOW) ? LOW : HIGH;
  prevState[3] = (curState[3] == LOW) ? LOW : HIGH;
  prevState[4] = (curState[4] == LOW) ? LOW : HIGH;

  Log.info("State has changed...");
  resetOutputs();
  delay(50);

  // Log.info("A:%d B:%d C:%d D:%d E:%d", inputs[0], inputs[1], inputs[2], inputs[3], inputs[4]);

  // MODE //
  if (curState[0] == HIGH && curState[1] == HIGH && curState[2] == HIGH && curState[3] == HIGH) {
    // Log.info("Received Instruction: Emergency Stop");
    digitalWrite(out[7], HIGH);
    return;
  }

  // TODO  PULSE CHECK

  // TURN //
  if (curState[0] == HIGH) {
    // Log.info("Received Instruction: Turn IN");
    digitalWrite(out[0], HIGH);
    digitalWrite(out[1], LOW);
  } else if (curState[1] == HIGH) {
    // Log.info("Received Instruction: Turn OUT");
    digitalWrite(out[0], LOW);
    digitalWrite(out[1], HIGH);
  }

  // FAN //
  if (curState[2] == HIGH && curState[3] == HIGH) {
    // Log.info("Received Instruction: Fan Fast");
    digitalWrite(out[2], LOW);
    digitalWrite(out[3], LOW);
    digitalWrite(out[4], HIGH);
  } else if (curState[2] == HIGH) {
    // Log.info("Received Instruction: Fan Slow");
    digitalWrite(out[2], HIGH);
    digitalWrite(out[3], LOW);
    digitalWrite(out[4], LOW);
  } else if (curState[3] == HIGH) {
    // Log.info("Received Instruction: Fan Med");
    digitalWrite(out[2], LOW);
    digitalWrite(out[3], HIGH);
    digitalWrite(out[4], LOW);
  }

  lastInputCheck = millis();
}

void resetOutputs() {
  for (size_t i = 0; i < numOut; i++) digitalWrite(out[i], LOW);
}

void cycleTest() {
  for (size_t i = 0; i < numOut; i++) {
    // Log.info("Testing output %d", i);
    digitalWrite(out[i], HIGH);
    delay(1000);
    digitalWrite(out[i], LOW);
  }
}